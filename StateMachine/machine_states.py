from state import State
from device import Device
import pigpio

pi = pigpio.pi()
#device = Device()
first_time_in_state = True
class StandBy(State):
    #standby state
    def run_state(self):
        #print(".")      
            
        pass

    def on_event(self, event):
        if event == 'valid_pulse':
            
            return Listening()
        
        return self

class Listening(State):
    #Listening state
    def run_state(self):
        #start countdown from 400
        if first_time_in_state:
            #tin = pi.get_current_tick()
            first_time_in_state = False
        
        else:
            #tnow = pi.get_current_tick()
            if tnow - tin > 400:
                device.on_event('timeout')


        pass

    def on_event(self, event):

        if event == 'valid_pulse':
            return B()
        
        if event == 'timeout':
            return A()

        return self

class A(State):
    # A signal detected
    #send info
    def run_state(self):
        #print("standby tasks")
        pass
    def on_event(self, event):
        if event == 'sent':
            return StandBy()
        
        return self
            
class B(State):
    # B signal detected
    # send info
    def run_state(self):
        #print("standby tasks")
        pass
    def on_event(self, event):
        if event == 'sent':
            return StandBy()

        return self
