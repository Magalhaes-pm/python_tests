from device import Device
import pigpio
import time
from threading import Timer

PW_MIN = 290000
PW_MAX = 310000
TIMEOUT = 400000

global timeout_t1
global timeout_t0
timeout_t0 = 0
timeout_t1 = 0

device = Device()

pi = pigpio.pi()

pi.set_mode(17,pigpio.INPUT)
pi.set_pull_up_down(17,pigpio.PUD_DOWN)

def cbf_s1_raising(gpio, level, tick):
    global raising
    raising = tick
    print('raising')
    return 

def cbf_s1_falling(gpio,level, tick1):
    global falling
    falling = tick1
    #pulse width:
    pulse_width = falling-raising
    print('falling')
    print(pulse_width)  #time in uS

    if pulse_width < PW_MAX and pulse_width > PW_MIN:
        #valid pulse
        device.on_event('valid_pulse')
        print('event sent')
           
    return 

cb1 = pi.callback(17, pigpio.RISING_EDGE,  cbf_s1_raising)
cb2 = pi.callback(17, pigpio.FALLING_EDGE, cbf_s1_falling)

while True:
    # Need to run the current state
    device.run_state()
    
  